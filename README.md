#Step 1 - Download Minikube and install (Linux)

> curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.30.0/minikube-linux-amd64 && chmod +x minikube && sudo mv minikube /usr/local/bin/

> minikube start

#Step 2 - Build Docker file
> bash build.sh
(Pushes Dockerfile to my repo currently)

#Step 3 - Run Terraform
> terraform init
> terraform apply

#Step 4 - Port forward
> bash port-forward.sh

Runs on port 8000, using pod to inject service account token for Kubernetes API - wasn't working correctly on replication controller

##To do
- Kuberneters /pods connecting to API via token but not displaying pods
- Add tests
- Automate minikube setup
