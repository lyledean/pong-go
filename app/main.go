package main

import (
	"fmt"
	"log"
	"net/http"
	"encoding/json"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)


func respondWithJson(w http.ResponseWriter, code int, object interface{}) {
	response, err := json.Marshal(object)
	if err != nil {
		panic(err)
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func connectToKubernetesAPI() *kubernetes.Clientset {
	config, _ := clientcmd.BuildConfigFromFlags("","")		
	// creates the clientset, add to handle errors
	clientset, _ := kubernetes.NewForConfig(config)
	return clientset
}

func main() {
	http.HandleFunc("/pods", func(w http.ResponseWriter, r *http.Request) {
		clientset := connectToKubernetesAPI()
		pods, _ := clientset.CoreV1().Pods("logging").List(metav1.ListOptions{})
		strings := make([]string, len(pods.Items))

		for i, pod := range pods.Items {
			strings[i] = pod.GetName()
		}
		respondWithJson(w, http.StatusCreated, strings)
	})
	http.HandleFunc("/volumes", func(w http.ResponseWriter, r *http.Request) {
		clientset := connectToKubernetesAPI()
		pvcs, _ := clientset.CoreV1().PersistentVolumeClaims("logging").List(metav1.ListOptions{})
		for i, pvc := range pvcs.Items {
			fmt.Printf("[%d] %s\n", i, pvc.GetName())
		}
		respondWithJson(w, http.StatusCreated, pvcs.Items)
	})

	http.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "pong")
	})

	log.Fatal(http.ListenAndServe(":8000", nil))
}