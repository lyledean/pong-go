resource "kubernetes_pod" "pong-go" {
  metadata {
    name = "pong-go"
    namespace = "kube-system"
  }

  spec {
    service_account_name = "${kubernetes_service_account.api.metadata.0.name}"
    container {
      image = "lyled/pong-go"
      name  = "pong-go"
      volume_mount {
        mount_path = "/var/run/secrets/kubernetes.io/serviceaccount"
        name = "${kubernetes_service_account.api.default_secret_name}"
        read_only = true
      }
    }
    volume {
      name = "${kubernetes_service_account.api.default_secret_name}"
      secret {
        secret_name = "${kubernetes_service_account.api.default_secret_name}"
      }
    }
  }
}


resource "kubernetes_service_account" "api" {
  metadata {
    name = "pong-go"
    namespace = "kube-system"
  }
}
